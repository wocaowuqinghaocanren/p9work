# -*- conding:UTF-8 -*-

'''
@ Project: p9django
@ File: luyou.py
@ Author: 孙博
@ Date：2022/7/14 18:49

'''
from rest_framework import routers
from django.urls import path
from .views import *

urlpatterns = [
    path('register/', Register.as_view()),
    path('login/', Login.as_view())
]

router = routers.DefaultRouter()
router.register('user', UserViewSet)
urlpatterns += router.urls