# Generated by Django 2.2.2 on 2022-07-14 11:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='uaccount',
            field=models.CharField(max_length=32, null=True, verbose_name='姓名拼音'),
        ),
        migrations.AlterField(
            model_name='user',
            name='uage',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='upwd',
            field=models.CharField(max_length=256, verbose_name='密码'),
        ),
        migrations.AlterField(
            model_name='user',
            name='usex',
            field=models.SmallIntegerField(choices=[(1, '男'), (2, '女')], default=1, null=True, verbose_name='性别'),
        ),
    ]
