# -*- conding:UTF-8 -*-

'''
@ Project: p9django
@ File: serializer.py
@ Author: 孙博
@ Date：2022/7/14 18:41

'''
from rest_framework import serializers
from .models import *


class UserSer(serializers.ModelSerializer):

    def get_sex_name(self, obj):
        return obj.get_usex_display()

    class Meta:
        model = User
        fields = '__all__'
