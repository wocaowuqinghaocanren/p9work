from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from django.contrib.auth.hashers import make_password, check_password
from rest_framework.response import Response
from .models import *
from .serializer import *


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSer


class Register(APIView):
    def post(self, request):
        uname = request.data.get('uname')
        pwd = request.data.get('upwd')
        uaccount = request.data.get('uaccount')
        # uage = request.data.get('uage')
        # usex = request.data.get('usex')
        hash_pwd = make_password(pwd)
        try:
            User.objects.create(uname=uname, upwd=hash_pwd, uaccount=uaccount)
            return Response({'message': '注册成功', 'code': 200})
        except Exception as e:
            print('注册失败', e)
            return Response({'message': '注册失败', 'code': 500})


class Login(APIView):
    def post(self, request):
        uaccount = request.data.get('uaccount')
        pwd = request.data.get('upwd')
        user = User.objects.filter(uaccount=uaccount).first()
        if user:
            if check_password(pwd, user.upwd):
                return Response({'message': '登录成功', 'code': 200})
            else:
                return Response({'message': '账号或密码错误', 'code': 500})
        else:
            return Response({'message': '该用户不存在', 'code': 500})
