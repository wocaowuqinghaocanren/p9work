from django.db import models


class User(models.Model):
    SEX_CHOICE = (
        (1, '男'),
        (2, '女')
    )
    uname = models.CharField('用户名称', max_length=32)
    uaccount = models.CharField('姓名拼音', max_length=32, null=True)
    upwd = models.CharField('密码', max_length=256)
    uage = models.IntegerField(null=True)
    usex = models.SmallIntegerField('性别', default=1, choices=SEX_CHOICE, null=True)

    def __str__(self):
        return self.uname

    class Meta:
        db_table = 'user'
        verbose_name_plural = '用户表'
